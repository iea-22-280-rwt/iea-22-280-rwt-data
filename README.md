# Repository for large datasets associated with the IEA 22 MW RWT

This repository contains datasets associated with the IEA 22 MW RWT
stored with Git LFS if binary or otherwise not meaningful to track changes
for. This repository is hosted on DTU Wind's GitLab instance since GitHub
charges for storage and traffic of Git LFS stored data.

## Contents

* structural_models
    * BECAS: BECAS cross-sectional meshes used to compute beam properties of the blade.
